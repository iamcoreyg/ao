$(document).ready(function(){
	var scrollPos = function() { return $(document).scrollTop() }

	var sizeVideo = function() { 
		$('.post iframe, .video-size').each(function(){
			$(this).css({
				'height': $(this).width()/1.7
			})
		})
	}
	sizeVideo()

	var breakpoints = ['320', '640', '978', '9999']

	var isBreak = function(i) {
		var  winWidth = $(window).width()
		if(winWidth > breakpoints[i] && winWidth < breakpoints[i+1]) {
			return true
		} else {
			return false
		}
	}

	var setContent = function() {
			if( isBreak(1) ) {
				//shift header
				$('#mini #title').css('width', '9%').html('<img src="images/ao_logo.png" />')
				$('#mini #mobile-social').css('padding', 0)

				//shift content
				$('#right-content').insertBefore('#left-content')
				$('#right-content').addClass("whole").removeClass("two-thirds")
				$('#left-content').addClass("whole").removeClass("one-third")
				$('#signup-wrap').toggleClass('hide')
				$('#left-content .grid').removeClass('grid').addClass('unit half')
			} else if( isBreak(2) ) {
				//shift headre
				$('#mini #title').removeAttr("style").text('Ashley Outrageous')
				$('#mini #mobile-social').css('padding', 0)

				//shift content
				$('#left-content').insertBefore('#right-content')
				$('#right-content').removeClass("whole").addClass("two-thirds")
				$('#left-content').removeClass("whole").addClass("one-third")
				$('#signup-wrap').toggleClass('hide')
				$('#left-content .half').addClass('grid').removeClass('unit half')

			}
	}

	setContent()

	$('#menu-icon a').click(function(){
		$('#standard').toggleClass('menu-down')

		if($('#menu-icon .fa').hasClass('fa-bars')) {
			$('#menu-icon .fa').addClass('fa-close').removeClass('fa-bars')
		} else {
			$('#menu-icon .fa').addClass('fa-bars').removeClass('fa-close')
		}
		return false
	})


	$(window).resize(function(){
		sizeVideo()
		setContent()
	})

	$(window).scroll(function(){
		if(scrollPos() > 100) {
			$("#mini").addClass('show-mini')
		} else {
			$("#mini").removeClass('show-mini')
		}
	})
})